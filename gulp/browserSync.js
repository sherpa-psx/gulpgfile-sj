var gulp          = require('gulp'),
    config        = require('../config'),
    browserSync   = require('browser-sync');

// Static Server + watching scss files

gulp.task('serve', config.tasks.serve, function(){
  browserSync.init({
    server: {
      baseDir: "./dist/"
    }
  });
  gulp.watch(config.styles.src + '**/*.scss', ['styles']);
  gulp.watch(config.scripts.src + '**/*.js', ['scripts']);
  gulp.watch([config.templates.src + '**/*.twig', config.templates.src + '*.json'], ['templates']);
});
