var gulp          = require('gulp'),
config        = require('../config');

gulp.task('copy', function() {
    gulp.src('./node_modules/bootstrap/scss/**/*.scss')
    .pipe(gulp.dest(config.styles.src + 'vendor/bootstrap'))
    gulp.src('./node_modules/bootstrap/dist/js/bootstrap.js')
    .pipe(gulp.dest(config.scripts.src + 'plugins'));
    gulp.src('./node_modules/sass-mq/_mq.scss')
    .pipe(gulp.dest(config.styles.src + 'vendor/'))
    gulp.src('./assets/icon-font/*')
    .pipe(gulp.dest('./dist/icon-font/'))
});
