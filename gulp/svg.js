var gulp          = require('gulp'),
    config        = require('../config'),
    svgmin        = require('gulp-svgmin'),
    svgstore      = require('gulp-svgstore'),
    rename        = require('gulp-rename');


var iconsPlugins = [{
    removeDimensions: true
}, {
    removeComments: true
}, {
    removeAttrs:  {attrs: '.*:(fill|stroke)'}
}];

gulp.task('svg-icons', function () {
    return gulp.src(config.svg.src + 'icons/*.svg')
        .pipe(svgmin({
            plugins: [{
                removeDimensions: true
            }, {
                removeComments: true
            }]
        }))
        .pipe(gulp.dest(config.svg.dist + 'icons/'));
});


gulp.task('svg-images', function() {
    gulp.src(config.svg.src + 'images/*.svg')
        .pipe(svgmin({
            plugins: [{
                removeDimensions: true
            }, {
                removeComments: true
            }]
        }))
        .pipe(svgstore())
        .pipe(rename({
            basename: 'svg-sprite'
        }))
        .pipe(gulp.dest(config.svg.dist + 'images/'));
});

gulp.task('svg', ['svg-icons','svg-images']);